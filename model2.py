# model2.py

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import normalize

from data_processing import *

data = pd.read_csv('nba1617.csv')
data = data_processing(data)

x_train,y_train,x_test,y_test = split_dataset(data,0.3,['homeWin','awayWin'])


model = RandomForestClassifier(n_estimators=100)
model.fit(normalize(x_train.values),y_train.values)

y_pred = model.predict(normalize(x_test.values))

print(accuracy_score(y_test.values, y_pred))