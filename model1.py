# model1.py

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense,Dropout

from data_processing import *

data = pd.read_csv('nba1617.csv')
data = data_processing(data)

x_train,y_train,x_test,y_test = split_dataset(data,0.1,['homeWin','awayWin'])

model = Sequential()
model.add(Dense(100, input_dim=x_train.shape[1], activation='relu'))
model.add(Dense(200, activation='relu'))
model.add(Dense(500, activation='relu'))
model.add(Dense(500, activation='relu'))
model.add(Dense(200, activation='relu'))
model.add(Dense(100, activation='relu'))
model.add(Dense(2,activation='softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(x_train.values,y_train.values, epochs=20, batch_size=16)

scores = model.evaluate(x_test.values,y_test.values)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))