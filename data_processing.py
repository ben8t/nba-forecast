import pandas as pd
from sklearn.model_selection import train_test_split

def data_processing(data):
	df = data.dropna(how='any',axis=0).copy()  # drop NA
	df['home'], df['away'] = df['game'].str.split(' - ',1).str  # split game to have home and away teams
	df['homeScore'], df['awayScore'] = df['score'].str.split(':',1).str  # split score to have home and away teams score
	df = df.assign(homeWin = (df.homeScore>df.awayScore).astype('int'),awayWin = (df.homeScore<df.awayScore).astype('int'))  # set homeWin and awayWin binary variable (to prediction)
	df.drop(['game','score','hour','homeScore','awayScore'], axis=1, inplace=True)
	df = pd.get_dummies(df, columns = ['home','away'])  # dummies for teams name
	return df

def split_dataset(df,split_rate,output_variable):
	"""
	Split dataset in 4 parts : x_train, y_train, x_test, y_test.
	INPUT : 
		split_rate : explicit
		output_variable : variables to predict

	OUTPUT : 
		x_train, y_train : train dataset to fit model.
		x_test : use for predictions
		y_test : use for error calculation
	"""
	train_set, test_set = train_test_split(df, test_size = split_rate)
	x_train  = train_set.drop(output_variable,axis=1)
	y_train = train_set[output_variable]
	x_test  = test_set.drop(output_variable,axis=1)
	y_test = test_set[output_variable]
	return x_train,y_train,x_test,y_test
